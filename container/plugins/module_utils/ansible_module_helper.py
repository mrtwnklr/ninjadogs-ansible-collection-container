from __future__ import absolute_import, division, print_function
from ansible.module_utils.basic import AnsibleModule
from jsonpath_ng import parse


class AnsibleModuleHelper:
    result = dict(
        changed=False,
        request="",
        response="",
    )

    def __init__(self, module: AnsibleModule):
        self.module = module
        self.params = self.module.params
        self.set_no_log_values("api_token", "password")

    def get_error_messages(self, response_data):
        error_messages = []

        expression = parse("$..errno")
        error_codes = [match.value for match in expression.find(response_data)]
        if error_codes is not None and len(error_codes) != 0:
            expression = parse("$..message")
            error_messages = [match.value for match in expression.find(response_data)]

        expression = parse("$..error")
        error_codes = [match.value for match in expression.find(response_data)]
        if error_codes is not None and len(error_codes) != 0:
            expression = parse("$..error")
            messages = [match.value for match in expression.find(response_data)]
            if messages[0] is not None:
                error_messages = messages

        return error_messages

    def has_changed(self, request, response):
        has_changed = False
        if len(request) > 0:
            all_values_same = all(
                response[key] == value for key, value in request.items()
            )
            has_changed = not all_values_same
        else:
            if len(response) > 0:
                has_changed = True

        return has_changed

    def handle_check_mode(self, changed, response):
        if self.module.check_mode:
            self.result["changed"] = changed
            self.result["response"] = response
            self.module.exit_json(**self.result)

    def exit_json(self):
        self.module.exit_json(**self.result)

    def fail_json(self, error_msg):
        self.module.fail_json(msg=error_msg, **self.result)

    def get_param(self, name):
        return self.params[name]

    def get_params(self):
        return self.module.params

    def set_request(self, request):
        self.result["request"] = request

    def set_response(self, response, fail_module=True):
        self.result["response"] = response
        self.check_for_error(response, fail_module)

    def check_for_error(self, data, fail_module=True):
        error_messages = self.get_error_messages(data)
        if len(error_messages) > 0 and fail_module:
            self.module.fail_json(msg=error_messages, **self.result)

    def set_no_log_values(self, *argument_names):
        for name in argument_names:
            try:
                value = self.get_param(name)
                self.module.no_log_values.add(value)
            except:
                pass

    def set_changed(self, changed):
        self.result["changed"] = changed

    @staticmethod
    def get_module(module_args, supports_check_mode=True):
        return AnsibleModule(
            argument_spec=module_args, supports_check_mode=supports_check_mode
        )
