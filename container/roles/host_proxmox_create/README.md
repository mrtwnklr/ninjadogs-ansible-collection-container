# ✨ host_cloud_init_create

This Ansible role provides a convenient way to create and update a virtual machine in Proxmox and tries to be idempotent.
The role variables are prefixed with `host_proxmox_` to make them compatible with the role `host_proxmox_delete`.
