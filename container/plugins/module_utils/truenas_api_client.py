import requests
import time
import json


class TruenasApiClient:
    def __init__(self, base_url, api_token, verify=True):
        self.base_url = base_url + "/api/v2.0/"
        self.api_token = api_token
        self.verify = verify
        self.headers = {"Authorization": f"Bearer {self.api_token}"}

    def get(self, endpoint, params={}):
        url = self.base_url + endpoint
        response = requests.get(
            url, headers=self.headers, verify=self.verify, params=params
        )
        return response.json()

    def post(self, endpoint, data):
        data = json.dumps(data)
        url = self.base_url + endpoint
        response = requests.post(
            url, headers=self.headers, verify=self.verify, data=data
        )
        return response.json()

    def put(self, endpoint, data):
        data = json.dumps(data)
        url = self.base_url + endpoint
        response = requests.put(
            url, headers=self.headers, data=data, verify=self.verify
        )
        return response.json()

    def delete(self, endpoint):
        url = self.base_url + endpoint
        response = requests.delete(url, headers=self.headers, verify=self.verify)
        return response.json()

    def find_item_by_property(self, api_endpoint, key, value):
        items = self.get(api_endpoint)
        for item in items:
            if item.get(key) == value:
                return item
        return None

    def wait_until_job_is_finished(self, job_id, timeout_sec):
        start_time = time.time()
        state = "RUNNING"
        is_success = False
        error_msg = ""
        params = {"id": job_id}

        while state == "RUNNING":
            if time.time() - start_time > timeout_sec:
                is_success = False
                error_msg = (
                    f"Something went wrong. The job was not finished after 20seconds."
                )

            jobs_data = self.get("core/get_jobs", params)
            if len(jobs_data) == 0:
                is_success = False
                error_msg = (
                    f"Something went wrong. There was no job with the id '{job_id}'."
                )
            elif len(jobs_data) == 1:
                job = jobs_data[0]
                try:
                    is_success = True
                    state = job["state"]
                except:
                    is_success = False
                    error_msg = (
                        f"Something went wrong. The job has not the attribute 'state'."
                    )

            else:
                is_success = False
                error_msg = f"Something went wrong. There were more than one job with the id '{job_id}'."

        return jobs_data, is_success, error_msg
