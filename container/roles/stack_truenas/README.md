# ✨ stack_truenas

This role was designed to create the necessary resources in truenas to isolate a stack.
The variables are prefixed with `stack_truenas__`.
