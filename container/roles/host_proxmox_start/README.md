# 🚀 host_proxmox_start

The Ansible role is specifically designed to start a virtual machines in Proxmox.
The role variables are prefixed with `host_proxmox__` to make them compatible with the variable from other `host_proxmox__` roles.
