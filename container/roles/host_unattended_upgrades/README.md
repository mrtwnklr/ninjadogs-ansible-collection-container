# host_unattended_upgrades

This role can be used to copy the unattended upgrades configuration templates.
The template is then configurable with the role variables.
