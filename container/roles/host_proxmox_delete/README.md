# 🔥 host_proxmox_delete

The Ansible role is specifically designed to delete virtual machines in Proxmox.
The role variables are prefixed with `host_proxmox__` to make them compatible with the variable from `host_proxmox_create`.
