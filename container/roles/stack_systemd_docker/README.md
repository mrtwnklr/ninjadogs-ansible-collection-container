# stack_systemd_docker

This role can be used to copy configuration files and docker templates to the host.
The docker container are running inside a systemd service.
That ensures that container restarts, even if the initial start fails.
The goal is to start a docker stack with as little configuration effort as possible.
See [defauts/main.yml](./defaults/main.yml) for further details.
